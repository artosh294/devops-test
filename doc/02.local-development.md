# ローカルの Kubernetes で開発する

## Run on Docker

- 2章から、このサンプルのコードをデプロイして試していく
- まずはDockerで動作確認

```
$ docker build -t gcr.io/project_name/devops-test web/.
$ docker run -it --rm --name web -p 80:80 gcr.io/project_name/devops-test
```

- ブラウザで http://localhost にアクセスできることを確認


## Deploy to Kubernetes

```
$ kubectl apply -f k8s/deployment.yaml
$ kubectl apply -f k8s/service.yaml
$ kubectl get pod
$ kubectl get deployment
$ kubectl get service
```

- 同じく、ブラウザで http://localhost にアクセスできることを確認

- クリーンアップ

```
$ kubectl delete service web
$ kubectl delete deployment web-production
```

-----


## Skaffoldを使って開発する

### Skaffold のインストール

- https://skaffold.dev/docs/getting-started/#installing-skaffold
- example: Instoll Skaffold to MacOS

```
$ brew install skaffold
```

- LinuxやWindowsの人は、上記のページから




### (オプション) Skaffold のサンプルを実行(Skaffold 公式に書いてある手順)
- Skaffold のサンプルを実行してみる(任意のディレクトリで実行してみる)

```
$ git clone https://github.com/GoogleContainerTools/skaffold
$ cd examples/getting-started
$ skaffold dev
```

- `main.go` を変更すると、コンテナイメージのビルド、k8sへのデプロイが自動で実行されることが確認できる




### Skaffold を使ってローカル開発を開始

```
$ docker rmi gcr.io/project_name/devops-test
$ skaffold dev
```

- `web/app.js` を編集すると、コンテナイメージのビルド、k8sへのデプロイが自動で実行されることが確認できる


### Dockerイメージのクリーンアップ

- Skaffoldは変更するたびに新しいDockerImageを作成するので、以下のようなコマンドで一括削除できる

```
$ docker rmi -f `docker images gcr.io/project_name/devops-test -q`
```


-----

## スケール

- `k8s/deployment.yaml` の `replicas: 1` の記述を `replicas: 3` に変更

```
$ kubectl get pod
$ kubectl apply -f k8s/deployment.yaml
$ kubectl get pod
```

- Podが3つになっている


## ローリングアップデート


- `web/app.js` の14行目の `greenyellow` を `silver` に変更
- 新しいイメージをビルド

```
$ docker build -t gcr.io/project_name/devops-test:v2 web/.
```

- `k8s/deployment.yaml` の `image: gcr.io/project_name/devops-test` の記述を `image: gcr.io/project_name/devops-test:v2` に変更

```
$ kubectl apply -f k8s/deployment.yaml
$ kubectl get pod
```

- ブラウザでアクセスすると、古いバージョンから新しいバージョン（背景色が違う）に切り替わっていく
- 古いイメージのPodが消えて新しいコンテナが起動する


**おつかれさまでした。次のセクションに進みます。**
